 ```sh
   vagrant up --no-provision \
     && vagrant provision --provision-with zookeeper \
     && vagrant provision --provision-with kafka \
     && ansible-playbook -i localhost playbook.yml
   ```

   docker run -p 8080:8080 \
	-e KAFKA_CLUSTERS_0_NAME=local \
	-e KAFKA_CLUSTERS_0_ZOOKEEPER=10.1.10.11:2181 \
  -e KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS=10.1.10.21:9092 \
	-d provectuslabs/kafka-ui:latest 
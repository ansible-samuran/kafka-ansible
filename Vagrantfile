def get_ip_kafka(index = 1)
  $ip_range_kafka.sub('xx', (index).to_s)
end

def get_ip_zookeeper(index = 1)
  $ip_range_zookeeper.sub('xx', (index).to_s)
end

$max_nodes_zookeeper = 2
$max_nodes_kafka = 2
$ip_range_zookeeper = '10.1.10.1xx'
$ip_range_kafka = '10.1.10.2xx'
$all_nodes_zookeeper = Array.new($max_nodes_zookeeper).fill { |j| "#{get_ip_zookeeper(j + 1)}" }
$all_nodes_kafka = Array.new($max_nodes_kafka).fill { |i| "#{get_ip_kafka(i + 1)}" }

$ansible_groups = {
  "kafka_node" => ["kafka-node[1:#{$max_nodes_kafka}]"],
  "zookeeper_node" => ["zookeeper[1:#{$max_nodes_zookeeper}]"],
  "all:vars" => {
    "vagrant_kafka_node_ips" => $all_nodes_kafka,
    "vagrant_zookeeper_ip" => $all_nodes_zookeeper
  }
}

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/focal64"

  (1..$max_nodes_zookeeper).each do |j|
    config.vm.define "zookeeper#{j}" do |zookeeper|
      zookeeper_ip_address = "#{get_ip_zookeeper(j)}"
      zookeeper.vm.network "private_network", ip: zookeeper_ip_address
      zookeeper.vm.hostname = "zookeeper#{j}"
    end
  end

  (1..$max_nodes_kafka).each do |i|
    config.vm.define "kafka-node#{i}" do |kafka|
      kafka_ip_address = "#{get_ip_kafka(i)}"
      kafka.vm.network "private_network", ip: kafka_ip_address
      kafka.vm.hostname = "kafka-node#{i}"
    end
  end

  config.vm.provision "zookeeper", type: "ansible", run: "never" do |ansible|
    ansible.playbook = "playbook-zookeeper.yml"
    ansible.groups = $ansible_groups
  end

  # Vault requires a running Consul cluster with an elected leader
  # Nomad in turn requires tokens from Vault
  # This playbook also includes the load balancer and DNS configuration
  config.vm.provision "kafka", type: "ansible", run: "never" do |ansible|
    ansible.playbook = "playbook.yml"
    ansible.groups = $ansible_groups
  end

  # Increase memory for Parallels Desktop
  config.vm.provider "parallels" do |p, o|
    p.memory = "1024"
  end

  # Increase memory for Virtualbox
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
  end

  # Increase memory for VMware
  ["vmware_fusion", "vmware_workstation"].each do |p|
    config.vm.provider p do |v|
      v.vmx["memsize"] = "1024"
    end
  end
end
